"use strict";

import "./styles/style.css";
import validate from "./email-validation.js";

const joinProgram = document.querySelector(".join-program");
const emailInput = document.getElementById("input-email");
const form = document.getElementById("form");
const submitButton = document.getElementById("btn");

const showSection = () => {
  joinProgram.style.display = "block";
};

const handleSubmit = (e) => {
  e.preventDefault();

  const email = emailInput.value;
  const isValid = validate(email);

  if (submitButton.textContent === "Unsubscribe") {
    emailInput.style.display = "block";
    emailInput.value = "";
    submitButton.textContent = "Subscribe";
    localStorage.removeItem("subscribed");
    localStorage.removeItem("email");
    return;
  }

  if (!isValid) {
    alert("Email is not valid");
    return;
  }

  localStorage.setItem("email", email);

  if (isValid) {
    emailInput.style.display = "none";
    submitButton.textContent = "Unsubscribe";
    localStorage.setItem("subscribed", "true");
  }

  emailInput.value = "";
};

submitButton.addEventListener("click", handleSubmit);

document.addEventListener("DOMContentLoaded", () => {
  if (localStorage.getItem("email")) {
    emailInput.value = localStorage.getItem("email");
    emailInput.style.display = "none";
    submitButton.textContent = "Unsubscribe";
  }
});

window.addEventListener("load", showSection);
form.addEventListener("submit", handleSubmit);
